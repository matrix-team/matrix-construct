Source: matrix-construct
Section: net
Priority: optional
Maintainer: Matrix Packaging Team <pkg-matrix-maintainers@lists.alioth.debian.org>
Uploaders:
 Jonas Smedegaard <dr@jones.dk>,
Build-Depends:
 debhelper-compat (= 13),
 dh-linktree,
 libboost-chrono1.71-dev (>= 1.66),
 libboost-context1.71-dev (>= 1.66),
 libboost-coroutine1.71-dev (>= 1.66),
 libboost1.71-dev (>= 1.66),
 libboost-filesystem1.71-dev (>= 1.66),
 libboost-system1.71-dev (>= 1.66),
 libbz2-dev,
 libgmp-dev,
 libgraphicsmagick++1-dev,
 libgraphicsmagick1-dev,
 libicu-dev,
 libjemalloc-dev,
 liblz4-dev,
 libmagic-dev,
 libmozjs-78-dev,
 libnss-db,
 librocksdb-dev (>= 6.11.4),
 librocksdb-dev (<< 6.11.5~),
 libsnappy-dev,
 libsodium-dev,
 libssl-dev,
 libzstd-dev,
 mesa-opencl-icd,
 opencl-dev,
 pandoc <!nodoc>,
 pkg-config,
Build-Depends-Indep:
 fonts-font-awesome,
 fonts-lato,
 libjs-angularjs,
 libjs-jquery-ui,
 libjs-jquery,
Standards-Version: 4.5.1
Homepage: https://github.com/matrix-construct/construct
Vcs-Git: https://salsa.debian.org/matrix-team/matrix-construct.git
Vcs-Browser: https://salsa.debian.org/matrix-team/matrix-construct
Rules-Requires-Root: no

Package: construct
Architecture: any
Depends:
 construct-common,
 ${misc:Depends},
 ${shlibs:Depends},
Description: homeserver for the Matrix protocol
 Construct is a fast and highly scalable Matrix homeserver.
 .
 Matrix is an open, federated communications protocol.

Package: construct-common
Architecture: all
Depends:
 ${misc:Depends},
Description: homeserver for the Matrix protocol - common files
 Construct is a fast and highly scalable Matrix homeserver.
 .
 Matrix is an open, federated communications protocol.
 .
 This package contains the common files
 that are used on all architectures without modification.

Package: construct-dev
Section: devel
Architecture: all
Depends:
 construct,
 ${misc:Depends},
Description: homeserver for the Matrix protocol - development headers
 Construct is a fast and highly scalable Matrix homeserver.
 .
 Matrix is an open, federated communications protocol.
 .
 This package contains development headers for Construct.
